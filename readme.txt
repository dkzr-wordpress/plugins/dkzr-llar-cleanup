=== Limit Login Attempts Reloaded database cleanup (by DKZR) ===
Contributors: joostdekeijzer
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=j@dkzr.nl&item_name=dkzr-llar-cleanup+WordPress+plugin&item_number=Joost+de+Keijzer&currency_code=EUR
Tags: Limit Login Attempts Reloaded, llar, database cleanup
Requires at least: 5.0
Tested up to: 6.4
Stable tag: 1.2
Requires PHP: 8.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Remove all data older than one year from LLAR logs.

== Description ==

Limit Login Attempts Reloaded can create huge WordPress option rows with many IPs in log entries. This plugin removes all IP data older than one year. A WP Cron job is run weekly to perform the cleanups.

Tested with Limit Login Attempts Reloaded v2.25.26

== Installation ==

* Download the plugin
* Uncompress it with your preferred unzip program
* Copy the entire directory in your plugin directory of your WordPress blog (/wp-content/plugins)
* Activate the plugin

== Changelog ==

= v1.2 =

* When IP is logged over a longer period in time, now old data is removed within the IP row.

= v1.1 =

* Added more cleanup

= v1.0 =

* Initial release

== Upgrade Notice ==
