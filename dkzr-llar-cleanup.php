<?php
/**
 * Plugin Name: Limit Login Attempts Reloaded database cleanup (by DKZR)
 * Plugin URI: https://dkzr.nl
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/llar-cleanup
 * Description: Remove all data older than one year from LLAR logs.
 * Author: Joost de Keijzer
 * Version: 1.2
 */

add_action( 'dkzr-llar-cleanup', function() {
  if ( ! class_exists( '\LLAR\Core\Config' ) ) {
    return;
  }

  $cutoff = time() - YEAR_IN_SECONDS;
  $ips = [];

  // handle 'logged' entry
  $data = \LLAR\Core\Config::get( 'logged' );
  if ( $data && is_array( $data ) ) {
    array_walk( $data, function( &$value ) use ( $cutoff ) {
      $value = array_filter( $value, function( $value ) use ( $cutoff ) {
        if ( is_scalar( $value ) || ! isset( $value['date'] ) ) {
          return false; // very old log item, remove
        } else {
          return $value['date'] > $cutoff;
        }
      } );
    } );
    $data = array_filter( $data );
    \LLAR\Core\Config::update( 'logged', $data );

    $ips = array_unique( array_merge( $ips, array_keys( $data ) ) );
  }

  // handle other entries with timestamps
  foreach( [ 'retries_valid', 'lockouts' ] as $config ) {
    $data = \LLAR\Core\Config::get( $config );
    if ( $data && is_array( $data ) ) {
      $data = array_filter( $data, function( $value ) use ( $cutoff ) { return $value > $cutoff; } );
      \LLAR\Core\Config::update( $config, $data );

      $ips = array_unique( array_merge( $ips, array_keys( $data ) ) );
    }
  }

  // handle 'retries' entry to only list current IPs in other settings
  $data = \LLAR\Core\Config::get( 'retries' );
  if ( $data && is_array( $data ) ) {
    foreach( array_keys( $data ) as $key ) {
      if ( ! in_array( $key, $ips, true ) ) {
        unset( $data[ $key ] );
      }
    }
    \LLAR\Core\Config::update( 'retries', $data );
  }
  unset( $ips );

  // handle 'retries_stats' entry
  $data = \LLAR\Core\Config::get( 'retries_stats' );
  if ( $data && is_array( $data ) ) {
    foreach( array_keys( $data ) as $key ) {
      if ( strtotime( $key ) < $cutoff ) {
        unset( $data[ $key ] );
      }
    }
    \LLAR\Core\Config::update( 'retries_stats', $data );
  }
} );

function dkzr_llar_cleanup_activate() {
  if ( wp_next_scheduled( 'dkzr-llar-cleanup' ) === false ) {
    wp_schedule_event( time(), 'weekly', 'dkzr-llar-cleanup' );
  }
}
register_activation_hook( __FILE__, 'dkzr_llar_cleanup_activate' );

function dkzr_llar_cleanup_deactivate() {
  $timestamp = wp_next_scheduled( 'dkzr-llar-cleanup' );
  wp_unschedule_event( $timestamp, 'dkzr-llar-cleanup' );
}
register_deactivation_hook( __FILE__, 'dkzr_llar_cleanup_deactivate' );
